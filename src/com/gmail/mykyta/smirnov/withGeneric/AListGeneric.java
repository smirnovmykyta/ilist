package com.gmail.mykyta.smirnov.withGeneric;

public class AListGeneric<T> implements IListGeneric {

    private T[] array;

    public AListGeneric(){
        array = (T[])new Object[10];
    }

    public AListGeneric(int capacity){
        array = (T[])new Object[capacity];
    }

    public AListGeneric(T[] array){
        array = (T[]) new Object[array.length + ((array.length / 100) * 20)];
        for(int i = 0; i < array.length; i++){
            array[i] = array[i];
        }
    }

    @Override
    public void clear(){
        array = (T[]) new Object[10];
    }

    @Override
    public int size(){
        return array.length;
    }

    @Override
    public Object get(int index){
        return array[index];
    }

    @Override
    public boolean add(Object value){
        int temp = 1;
        try{
            T[] tempObjectArray = (T[]) new Object[array.length + 1];
            for(int i = 0; i < array.length; i++){
                tempObjectArray[i] = array[i];
            }

            for(int i = 0; i < tempObjectArray.length; i++){
                if (tempObjectArray[i] == null){
                    tempObjectArray[i] = (T)value;
                    break;
                }
                temp++;
            }

            array = (T[]) new Object[temp + ((temp / 100) * 20)];

            for (int i = 0; i < array.length; i++){
                array[i] = tempObjectArray[i];
            }
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean add(Object value, int index){
        if(array.length - 1 < index || index < 0){
            return false;
        }

        int temp = 1;
        T[] temparray = (T[]) new Object[array.length + 1];
        for(int i = 0; i <= index; i++){
            if(i == index){
                temparray[i] = (T)value;
            }
            temparray[i] = array[i];
            temp++;
        }

        for(int i = index; i < array.length; i++){
            temparray[i + 1] = array[i];
            temp++;
        }

        array = (T[]) new Object[temp + ((temp / 100) * 20)];

        for (int i = 0; i < array.length; i++){
            array[i] = temparray[i];
        }

        return true;
    }

    @Override
    public T remove(Object value){
        for(int i = 0; i < array.length; i++){
            if(array[i] == value){
                array[i] = null;
            }
        }
        return (T) value;
    }

    @Override
    public T removeByIndex(int index) {
        T temp = null;
        for(int i = 0; i < array.length; i++){
            if(i == index){
                temp = array[i];
                array[i] = null;
            }
        }
        return (T)temp;
    }

    @Override
    public boolean contains(Object value) {
        for(int i = 0; i < array.length; i++){
            if(array[i] == value){
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        String resultForPrint = "[";
        for(int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                resultForPrint = resultForPrint + array[i] + "]";
            } else {
                resultForPrint = resultForPrint + array[i] + ",";
            }
        }
        System.out.println(resultForPrint);
    }

    @Override
    public T[] toArray() {
        T[] resultArray = (T[]) new Object[array.length];
        for (int i = 0; i < array.length; i++){
            if(array[i] != null){
                resultArray[i] = array[i];
            }
        }
        return resultArray;
    }

    @Override
    public T[] subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(Object[] arr) {
        try {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (array[i] == arr[j]) {
                        array[i] = null;
                    }
                }
            }
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean retainAll(Object[] arr) {
        T[] temp = (T[]) new Object[array.length];
        int tempIndex = 0;
        try {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (array[i] == arr[j]) {
                        temp[tempIndex] = array[i];
                        tempIndex++;
                    }
                }
            }
            array = (T[]) new Object[temp.length];
            for (int i = 0; i < array.length; i++){
                array[i] = temp[i];
            }
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }
}

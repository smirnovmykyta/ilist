package com.gmail.mykyta.smirnov.withoutGeneric;

public class AList implements IList {


    static Integer[] integer;

    public AList(){
         integer = new Integer[10];
    }

    public AList(int capacity){
         integer = new Integer[capacity];
    }

    public AList(int[] array){
         integer = new Integer[array.length + ((array.length / 100) * 20)];
        for(int i = 0; i < array.length; i++){
            integer[i] = array[i];
        }
    }

    @Override
    public void clear(){
         integer = new Integer[10];
    }

    @Override
    public int size(){
        return integer.length;
    }

    @Override
    public int get(int index){
        return integer[index];
    }

    @Override
    public boolean add(int number){
        int temp = 1;
        try{
        Integer[] tempIntegerArray = new Integer[integer.length + 1];
        for(int i = 0; i < integer.length; i++){
            tempIntegerArray[i] = integer[i];
        }

        for(int i = 0; i < tempIntegerArray.length; i++){
            if (tempIntegerArray[i] == null){
                tempIntegerArray[i] = number;
                break;
            }
            temp++;
        }

        integer = new Integer[temp + ((temp / 100) * 20)];

        for (int i = 0; i < integer.length; i++){
            integer[i] = tempIntegerArray[i];
        }
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean add(int number, int index){
        if(integer.length - 1 < index || index < 0){
            return false;
        }

        int temp = 1;
        Integer[] tempInteger = new Integer[integer.length + 1];
        for(int i = 0; i <= index; i++){
            if(i == index){
                tempInteger[i] = number;
            }
            tempInteger[i] = integer[i];
            temp++;
        }

        for(int i = index; i < integer.length; i++){
            tempInteger[i + 1] = integer[i];
            temp++;
        }

        integer = new Integer[temp + ((temp / 100) * 20)];

        for (int i = 0; i < integer.length; i++){
            integer[i] = tempInteger[i];
        }

        return true;
    }

    @Override
    public int remove(int number){
        for(int i = 0; i < integer.length; i++){
            if(integer[i] == number){
                integer[i] = null;
                return 1;
            }
        }

        return 0;
    }

    @Override
    public int removeByIndex(int index) {
        for(int i = 0; i < integer.length; i++){
            if(i == index){
                integer[i] = null;
                return 1;
            }
        }
        return 0;
    }

    @Override
    public boolean contains(int number) {
        for(int i = 0; i < integer.length; i++){
            if(integer[i] == number){
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        String resultForPrint = "[";
        for(int i = 0; i < integer.length; i++) {
            if (i == integer.length - 1) {
                resultForPrint = resultForPrint + integer[i] + "]";
            } else {
                resultForPrint = resultForPrint + integer[i] + ",";
            }
        }
        System.out.println(resultForPrint);
    }

    @Override
    public int[] toArray() {
        int[] resultArray = new int[integer.length];
        for (int i = 0; i < integer.length; i++){
            if(integer[i] != null){
                resultArray[i] = integer[i];
            }
        }
        return resultArray;
    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(int[] arr) {
        try {
            for (int i = 0; i < integer.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (integer[i] == arr[j]) {
                        integer[i] = null;
                    }
                }
            }
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean retainAll(int[] arr) {
        Integer[] temp = new Integer[integer.length];
        int tempIndex = 0;
        try {
            for (int i = 0; i < integer.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (integer[i] == arr[j]) {
                        temp[tempIndex] = integer[i];
                        tempIndex++;
                    }
                }
            }
            integer = new Integer[temp.length];
            for (int i = 0; i < integer.length; i++){
                integer[i] = temp[i];
            }
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }


}

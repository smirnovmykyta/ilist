package withoutGeneric;

import com.gmail.mykyta.smirnov.withoutGeneric.AList;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class AListTest {
    @Test
    void clear_ShouldDeleteAllElements() {

        //given
        int[] list = {1, 2, 3};
        AList aList = new AList(list);

        //when
        aList.clear();

        //then
        assertEquals(10, aList.size());
        assertNotEquals(0, aList.size());
    }


    @Test
    void size_ShouldReturnSize() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);

        //when
        int expected = 3;

        //then
        assertEquals(expected, aList.size());
        assertNotEquals(0, aList.size());
    }

    @Test
    void get_ShouldReturnElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);
        int index = 2;

        //when
        int expected = 3;

        //then
        assertEquals(expected, aList.get(index));
        assertNotEquals(2, aList.get(index));
    }

    @Test
    void add_ShouldAddElement() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = aList.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void add_ShouldAddElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);
        int number = 2;
        int index = 1;

        //when
        boolean expected = true;
//        boolean actual = aList.add(index, number);

        //then
//        assertEquals(expected, actual);
//        assertNotEquals(0, actual);
    }

    @Test
    void remove_ShouldRemoveElement() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);
        int number = 2;

        //when
        int actual = aList.remove(number);

        //then
        assertEquals(1, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void removeByIndex_ShouldRemoveElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);
        int index = 1;

        //when
        int actual = aList.removeByIndex(index);

        //then
        assertEquals(index, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void contains_ShouldCheckElement() {

        //given
        int list[] = {1, 2, 3};
        AList aList = new AList(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = aList.contains(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void toArray_ShouldGetListAsAnArray() {

        //given
        int[] list = {1, 2, 3};
        AList aList = new AList(list);

        //when
        int[] expected = {1, 2, 3};
        int[] actual = aList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, Arrays.toString(actual));
    }

    @Test
    void removeAll_ShouldGetAllElementsWhichAreInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        AList aList = new AList(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = aList.removeAll(arr);

        //then
//        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void retainAll_ShouldGetAllElementsWhichDoNotRepeatedInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        AList aList = new AList(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = aList.retainAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }
}

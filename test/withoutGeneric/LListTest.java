package withoutGeneric;

import com.gmail.mykyta.smirnov.withoutGeneric.LList;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LListTest {

    @Test
    void clear_ShouldDeleteAllElements() {
    }

    @Test
    void size_ShouldReturnSize() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);

        //when
        int expected = 3;

        //then
        assertEquals(expected, lList.size());
        assertNotEquals(0, lList.size());
    }

    @Test
    void get_ShouldReturnElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int index = 2;

        //when
        int expected = 3;

        //then
        assertEquals(expected, lList.get(index));
        assertNotEquals(2, lList.get(index));
    }

    @Test
    void add_ShouldAddElement() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lList.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void add_ShouldAddElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int number = 2;
        int index = 1;

        //when
        boolean expected = true;
        boolean actual = lList.add(index, number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void remove_ShouldRemoveElement() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int number = 2;

        //when
        int actual = lList.remove(number);

        //then
        assertEquals(1, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void removeByIndex_ShouldRemoveElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int index = 1;

        //when
        int actual = lList.removeByIndex(index);

        //then
        assertEquals(index, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void contains_ShouldCheckElement() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lList.contains(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void toArray_ShouldGetListAsAnArray() {

        //given
        int[] list = {1, 2, 3};
        LList lList = new LList(list);

        //when
        int[] expected = {1, 2, 3};
        int[] actual = lList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, Arrays.toString(actual));
    }

    @Test
    void removeAll_ShouldGetAllElementsWhichAreInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        LList lList = new LList(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = lList.removeAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void retainAll_ShouldGetAllElementsWhichDoNotRepeatedInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        LList lList = new LList(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = lList.retainAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void addStart() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lList.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void addEnd() {

        //given
        int list[] = {1, 2, 3};
        LList lList = new LList(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lList.addLast(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }
}

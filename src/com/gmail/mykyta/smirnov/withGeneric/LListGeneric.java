package com.gmail.mykyta.smirnov.withGeneric;

public class LListGeneric<T> implements IListGeneric {
    private Node head;
    private Node end;
    private int size;


    public LListGeneric() {
    }

    public LListGeneric(Object[] array) {
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }

    public boolean addStart(Object value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;

        } else {
            newNode.setNext(head);
            head = newNode;
        }
        size++;
        return true;
    }

    public boolean addLast(Object value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;
        } else {
            Node current = head;
            while (null != current.getNext()) {
                current = current.getNext();
            }
            current.setNext(newNode);
        }
        size++;
        return true;
    }

    @Override
    public void clear() {
        head = null;
        end = null;
        size = 0;

    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public T get(int index) {
        Node value = head;
        for (int i = 0; i < index; i++) {
            value = value.getNext();
        }
        return (T)value.getVal();
    }

    @Override
    public boolean add(Object value) {
        if (head == null) {
            head = new Node(value);
        } else {
            Node node = head;
            while (node.getNext() != null) {
                node = node.getNext();
            }
            node.setNext(new Node(value));
        }
        size++;
        return true;
    }

    @Override
    public boolean add(Object value, int index) {
        Node node = new Node(value);
        if (index == 0) {
            addStart(value);
        } else {
            Node temp = head;
            for (int i = 0; i < index - 1; i++) {
                temp = temp.getNext();
            }
            Node tempNode = temp.getNext();
            node.setNext(tempNode);
            temp.setNext(node);
        }
        size++;
        return true;
    }

    @Override
    public T remove(Object value) {
        Node current = head;
        Node temp = null;
        if (current != null && current.getVal() == value) {
            head = current.getNext();
            return (T) head;
        }
        while (current != null && current.getVal() != value) {
            temp = current;
            current = current.getNext();
        }
        if (current == null) {
            return null;
        }
        temp.setNext(current.getNext());
        size--;
        return (T)current;

    }

    @Override
    public T removeByIndex(int index) {
        try {
            if (index == 0) {
                Node temp = head;
                head = head.getNext();
                temp.setNext(null);
                size--;
                return (T)head;
            } else {
                Node current = head;
                int count = 0;
                while (count < index - 1) {
                    current = current.getNext();
                    count++;
                }
                Node value = current.getNext();
                current.setNext(current.getNext());
                value.setNext(null);
                size--;
                return (T)current;
            }


        } catch (NullPointerException e) {
            return null;
        }

    }

    @Override
    public boolean contains(Object value) {
        Node current = head;
        if (current.getVal() == value) {
            return true;
        }
        while (current.getNext() != null) {

            current = current.getNext();
            if (current.getVal() == value) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        String listForPrint = "[";

        if (head == null) {
            listForPrint = listForPrint + "]";
        } else {
            Node temp = head;

            while (temp != null) {
                if (temp.getNext() == null) {
                    listForPrint = listForPrint + temp.getVal() + "]";
                } else {
                    listForPrint = listForPrint + temp.getVal() + ",";
                }
                temp = temp.getNext();
            }
        }
        System.out.println(listForPrint);

    }

    @Override
    public T[] toArray() {
        T[] array = (T[]) new Object[size];
        Node tempNode = head;
        int temp = 0;
        while (tempNode != null) {
            array[temp] = (T)tempNode.getVal();
            tempNode = tempNode.getNext();
            temp++;
        }
        return array;

    }

    @Override
    public T[] subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(Object[] arr) {
        try {
            for (Object value : arr) {
                remove(value);
            }
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean retainAll(Object[] arr) {
        try {
            for (Object value : arr) {
                if (!contains(value)) {
                    add(value);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

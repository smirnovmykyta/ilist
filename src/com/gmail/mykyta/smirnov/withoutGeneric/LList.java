package com.gmail.mykyta.smirnov.withoutGeneric;

public class LList implements IList {

    private Node head;
    private Node end;
    private int size;


    public LList() {
    }

    public LList(int[] array) {
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }

    public boolean addStart(int value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;

        } else {
            newNode.setNext(head);
            head = newNode;
        }
        size++;
        return true;
    }

    public boolean addLast(int value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;
        } else {
            Node current = head;
            while (null != current.getNext()) {
                current = current.getNext();
            }
            current.setNext(newNode);
        }
        size++;
        return true;
    }

    @Override
    public void clear() {
        head = null;
        end = null;
        size = 0;

    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        Node value = head;
        for (int i = 0; i < index; i++) {
            value = value.getNext();
        }
        return value.getVal();
    }

    @Override
    public boolean add(int number) {
        if (head == null) {
            head = new Node(number);
        } else {
            Node node = head;
            while (node.getNext() != null) {
                node = node.getNext();
            }
            node.setNext(new Node(number));
        }
        size++;
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        Node node = new Node(number);
        if (index == 0) {
            addStart(number);
        } else {
            Node temp = head;
            for (int i = 0; i < index - 1; i++) {
                temp = temp.getNext();
            }
            Node tempNode = temp.getNext();
            node.setNext(tempNode);
            temp.setNext(node);
        }
        size++;
        return true;
    }

    @Override
    public int remove(int number) {
        Node current = head;
        Node temp = null;
        if (current != null && current.getVal() == number) {
            head = current.getNext();
            return 1;
        }
        while (current != null && current.getVal() != number) {
            temp = current;
            current = current.getNext();
        }
        if (current == null) {
            return -1;
        }
        temp.setNext(current.getNext());
        size--;
        return 1;

    }

    @Override
    public int removeByIndex(int index) {
        try {
            if (index == 0) {
                Node temp = head;
                head = head.getNext();
                temp.setNext(null);
            } else {
                Node current = head;
                int count = 0;
                while (count < index - 1) {
                    current = current.getNext();
                    count++;
                }
                Node value = current.getNext();
                current.setNext(current.getNext());
                value.setNext(null);
            }
            size--;
            return 1;
        } catch (NullPointerException e) {
            return 0;
        }

    }

    @Override
    public boolean contains(int number) {
        Node current = head;
        if (current.getVal() == number) {
            return true;
        }
        while (current.getNext() != null) {

            current = current.getNext();
            if (current.getVal() == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        String listForPrint = "[";

        if (head == null) {
            listForPrint = listForPrint + "]";
        } else {
            Node temp = head;

            while (temp != null) {
                if (temp.getNext() == null) {
                    listForPrint = listForPrint + temp.getVal() + "]";
                } else {
                    listForPrint = listForPrint + temp.getVal() + ",";
                }
                temp = temp.getNext();
            }
        }
        System.out.println(listForPrint);

    }

    @Override
    public int[] toArray() {
        int[] array = new int[size];
        Node tempNode = head;
        int temp = 0;
        while (tempNode != null) {
            array[temp] = tempNode.getVal();
            tempNode = tempNode.getNext();
            temp++;
        }
        return array;

    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(int[] arr) {
        try {
            for (int value : arr) {
                remove(value);
            }
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean retainAll(int[] arr) {
        try {
            for (int value : arr) {
                if (!contains(value)) {
                    add(value);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

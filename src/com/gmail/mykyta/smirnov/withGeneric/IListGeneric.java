package com.gmail.mykyta.smirnov.withGeneric;

import java.util.List;

public interface IListGeneric<T> {
    void clear();
    int size();
    T get(int index);
    boolean add(T  value);
    boolean add(T  value, int index);
    T remove(T  value);
    T removeByIndex(int index);
    boolean contains(T  value);
    void print();
    T[] toArray();
    T[] subList(int fromIdex, int toIndex);
    boolean removeAll(T[] arr);
    boolean retainAll(T[] arr);
}
